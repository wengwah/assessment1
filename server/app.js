// Server side javascript code

console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname + "/../client/"));


// check if server data passes into server
app.get("/hello", function (req, res) {
  res.status(200).json("Hello World"); 
});

// Receive from client to be POSTed to server
app.post("/members", function(req, res) {
  console.log("Received member object " + JSON.stringify(req.body));

  var member = req.body;
  
  // To be printed on console
  console.log("email > " + member.email);
  console.log("password > " + member.password);
  console.log("Name > " + member.username);
  console.log("Gender > " + member.gender);
  console.log("DOB > " + member.dateOfBirth);
  console.log("Address > " + member.address);
  console.log("Nationality > " + member.nationality);
  console.log("Contact Number > " + member.contactNumber);

  res.status(200).json(member);
});


app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});

module.exports = app;