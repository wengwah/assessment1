##Assessment 1 Readme

###Testing for Cloud Deployment 6 Oct FAILED

  * Created pipeline YAML file
  * Created appspec YAML file and cloud deploy scripts
  * YAML scripts ran as expected failing at aws push
  * Created new S3 bucket, new IAM role and group tagged to existing user, and new EC2 instance
  * "Unable to assume role" as I tried to codedeploy the application

###Final commit @ 8.00pm

  * I wish to thank Kenneth for accomodating my medical appt
  * Removed some test binding variables from browser
  * Removed unnecessary validation fields

####Unresolved

  * Validation (for dob) does not prevent form from sending all data fields to server
  * Minor issues with contact number regex
  * Minor unexpected behaviours with bootstrap

####Closed

  * Responsive display completed

###5th commit @ 7.00pm

  * Minor testing done

####Open

  * Validation does not prevent form from sending all data fields to server (for dob and gender)
  * Minor issues with contact number regex
  * Bootstrap formating of form for responsive display

####Closed

  * Submit button properly propagates data to server
  * Submit button properly propagates message to browser
  

###4th commit @ 6.00pm

  * Resume coding

###3rd commit @ 2.00pm

  * Email validation bug fixed
  * Submit button on client needs update
  * HTML format using Bootstrap needs adjustments
  * Message needs to be printed
  * Server should receive data after submit button is pressed 

###2nd commit @ 1.00 pm

  * All fields mapped
  * Found bug with email pattern validation
  * Password check function does not work, introduced workaround in html (Resolved)
  * Age validation does not work (Resolved)
  * Contact number Regex needs update
  * Need to test submit and post function
  * Need to print message

###1st commit @ 10.30 am

  * scafolding done
  * no other issues 