#!/bin/bash
WENGWAH_ASSESSMENT1_HOME=$HOME/wengwah-assessment1

if [ -d "$WENGWAH_ASSESSMENT1_HOME" ]; then
   zip -r $HOME/backup/$(date +"%m-%d-%Y").zip $WENGWAH_ASSESSMENT1_HOME/
fi
rm -rf $WENGWAH_ASSESSMENT1_HOME/*
rm -rf $WENGWAH_ASSESSMENT1_HOME/.git