(function() {
  "use strict";
  angular
    .module("RegApp")
    .controller("RegAppCtrl", RegAppCtrl);

  RegAppCtrl.$inject = ["$http", "$log", "$q"];

  function RegAppCtrl ($http) {
    
    // initializing variables for testing
    var self = this;
    self.helloworld = "";

    self.member = {
      email: "",
      password: "",
      confirmPassword: "",
      Name: "",
      gender: "",
      dateOfBirth: "",
      address: "",
      nationality: "",
      contactNumber: ""
    };

    self.displayMember = {
      email: "",
      password: "",
      confirmPassword: "",
      Name: "",
      gender: "",
      dateOfBirth: "",
      address: "",
      nationality: "",
      contactNumber: ""
    };

    self.thanks = "";

    // for testing
    $http.get("/hello")
      .then(function (success) {
        console.log(success);
        self.helloworld = success.data;
      }).catch(function (error) {
        console.log(error);
      })

    self.registerMember = function() {

      $http.post("/members", self.member)
        .then(function (success) {
          console.log(success);
          self.thanks = "All done! Thanks for submitting.";
        })
        .catch(function (error) {
          console.log(error);
        })
    };

// Abandoned
    // function passwordCheck() {
    //   if (member.password != member.confirmPassword) {
    //     return true;
    //   } else {
    //     return false;
    //   }
    // };

    self.ageValid = function(dateOfBirth) {            
      var minAge = 18;
      var ageDifMs = Date.now() - dateOfBirth.getTime();
      var ageDate = new Date(ageDifMs); // miliseconds from epoch
      var ageYear = Math.abs(ageDate.getUTCFullYear() - 1970);
       
      console.log(ageYear);
      if(ageYear >= minAge){
          return true;
      } else {
           return false;
      }
    };

  }; /* RegAppCtrl function end */

}) ();